#include "main.h"
int main()
{
    COULEUR_CONSOLE_BLANC_BLEU();

//variables
    int choixMenu = 0;
    char coorC = NULL;
    int coorX = 0, coorY = 0;
    int touche = 0;
    char tabLettre[] = {' ','A','B','C','D','E','F','G','H','I','J'} ;
    int compteurTir = 0;
    int debordement = 1;
//Structures
    Joueur humain = {"JOUEUR DEFAULT",{},{},{0},0,0,0};
    Joueur ordinateur = {"ORDINATEUR",{},{},{0},0,0,0};
// Variables strat�gie
    int strategieEnCours = 0;
    int coorXinitiale = 0,  coorYinitiale = 0;
    int directionTir = 0, dirX = 0, dirY = 0;
    int toucheStrategie = 0;
    int resteT = 0;
    srand(time(NULL));
//Variables pour jouer 25 fois
    int tabStockCoups[10] = {}; // CoorX stock [0], CoordY stock [1] * ( bateaux
    int difficulte = 0;
    int stockageCoup = 1;
    int compteWhileStockCoup = 0;
    int compteWhilePlayStockCoup = 2;

    while (EXECUTE_WHILE)   //Ex�cution du programme tant que l'utilisateur ne quitte pas en saisissant 5
    {
        if (choixMenu == 0)
        {
            menu(&choixMenu);   //Affichage du menu g�n�ral
        }

        switch (choixMenu) // Switch de la s�lection du menu principal
        {
        case PARTIE_RAPIDE :
            ordinateur.nbrParties = humain.nbrParties;
            ordinateur.nbrPartiesGagnees = humain.nbrParties - humain.nbrPartiesGagnees;
            //Initialisation des variables, utile lorsque l'on recomme une partie apr�s en avoir gagn� une
            //A FAIRE Initialisation des structures A FAIRE
            touche = 0;
            compteurTir = 0;
            debordement = 1;
            strategieEnCours = 0;
            coorXinitiale = 0;
            coorYinitiale = 0;
            directionTir = 0;
            dirX = 0;
            dirY = 0;
            toucheStrategie = 0;
            resteT = 0;
            compteWhileStockCoup = 0;
            compteWhilePlayStockCoup = 2;
            stockageCoup = 1;
            initTabStockCoups(tabStockCoups);
            strategieEnCours = 0;
            humain.gagne = 0;
            ordinateur.gagne = 0;
            compteurTir = 0;
            initTabCoule(humain.tabBatCoule);
            initTabCoule(ordinateur.tabBatCoule);
            initGrille(humain.tabGrille);
            initGrille(ordinateur.tabGrille);
            initBateau(humain.tabBateau);
            initBateau(ordinateur.tabBateau);
            creationDesBateaux(humain.tabBateau);
            creationDesBateaux(ordinateur.tabBateau);

            difficulte = selectionDifficulte();

            while (humain.gagne != 1 && ordinateur.gagne != 1)
            {
                compteurTir++;
//==============Affichage du plateau de jeu
                afficherGrille(humain,ordinateur,tabLettre,compteurTir);
//==============TOUR DU JOUEUR 1
                while(debordement == 1) // Pour tester si le joueur n'a pas d�j� saisie des coordonn�es.
                {
                    saisieDesCoordonnees(&coorC,&coorY,tabLettre);
                    if (coorC == 'Q')//Quitte la partie en cours si le joueur saisi Q...
                    {
                        choixMenu = 0;
                        break;
                    }
                    coorX = transformCoorCharToInt(tabLettre,coorC);
                    debordement = testDebordement(coorX,coorY,ordinateur.tabGrille);
                    if (debordement == 1)//
                    {
                        printf("\n Coordonnees saisies en dehors de la grille ou deja jouees.  \n");
                    }
                    else
                    {
                        debordement=1;
                        break;
                    }
                }
                if (coorC == 'Q')//Quitte la partie en cours si le joueur saisi Q...
                {
                    break;
                }
                touche = tir(coorX,coorY,ordinateur.tabBateau,ordinateur.tabGrille,ordinateur.tabBatCoule);
                if (touche == 1) // Appel des fonctions uniquement si le tir est concluant
                {
                    effetVertBlanc();
                    humain.gagne = verifGagne(ordinateur.tabBatCoule);
                }
//==============TOUR ORDINATEUR POUR STOCKER COUPS
                while(stockageCoup == 1 && compteurTir == 1 && difficulte != 0)
                {
                    if(strategieEnCours != 0)
                    {
                        strategieOrdinateur(&coorX, &coorY,&coorXinitiale,&coorYinitiale,strategieEnCours,toucheStrategie,humain.tabGrille,humain.tabBateau,&directionTir,&dirX,&dirY,humain.tabBatCoule);
                        strategieEnCours++;
                    }
                    else
                    {
                        coordonneesAleatoire(&coorX, &coorY, humain.tabGrille);
                    }
                    touche = tir(coorX,coorY,humain.tabBateau,humain.tabGrille,humain.tabBatCoule);
                    toucheStrategie = touche; // Sert losque l'on est dans la strat�gie, si on touche on continue dans la direction, sinon on revient dans la direction inverse
                    if(touche == 1)
                    {
                        if(strategieEnCours == 0)
                        {
                            strategieEnCours = 1;
                            coorXinitiale = coorX;
                            coorYinitiale = coorY;
                        }
                        if (humain.tabBatCoule[humain.tabBateau[coorX][coorY]]==9) // Si le bateau des coordonn�es en cours est coul�, on sort de la strat�gie
                        {
                            strategieEnCours = 0;
                            if(difficulte>=compteWhileStockCoup)
                            {
                                tabStockCoups[compteWhileStockCoup] =  coorXinitiale;
                                tabStockCoups[compteWhileStockCoup+1] =  coorYinitiale;
                                compteWhileStockCoup=compteWhileStockCoup+2;
                                if(difficulte==compteWhileStockCoup)
                                {
                                    break;
                                }
                            }
                            resteT = resteTinGrille(humain.tabGrille,&coorXinitiale,&coorYinitiale);
                            if(resteT == 1)
                            {
                                strategieEnCours = 1;
                            }
                        }
                    }
                }
//==============TOUR ORDINATEUR
                if(compteurTir == 1 && difficulte != 0)
                {
                    initTabCoule(humain.tabBatCoule);
                    initGrille(humain.tabGrille);
                    coorX = tabStockCoups[0];
                    coorY = tabStockCoups[1];
                }
                else
                {
                    if(difficulte>compteWhilePlayStockCoup)
                    {
                        coorX = tabStockCoups[compteWhilePlayStockCoup];
                        coorY = tabStockCoups[compteWhilePlayStockCoup+1];
                        compteWhilePlayStockCoup = compteWhilePlayStockCoup + 2;
                    }
                    else
                    {
                        if(strategieEnCours != 0)
                        {
                            strategieOrdinateur(&coorX, &coorY,&coorXinitiale,&coorYinitiale,strategieEnCours,toucheStrategie,humain.tabGrille,humain.tabBateau,&directionTir,&dirX,&dirY,humain.tabBatCoule);
                            strategieEnCours++;
                        }
                        else
                        {
                            coordonneesAleatoire(&coorX, &coorY, humain.tabGrille);
                        }
                    }
                }
                touche = tir(coorX,coorY,humain.tabBateau,humain.tabGrille,humain.tabBatCoule);
                toucheStrategie = touche; // Sert losque l'on est dans la strat�gie, si on touche on continue dans la direction, sinon on revient dans la direction inverse
                if(touche == 1)
                {
                    effetVioletBlanc();
                    ordinateur.gagne = verifGagne(humain.tabBatCoule);

                    if(strategieEnCours == 0)
                    {
                        strategieEnCours = 1;
                        coorXinitiale = coorX;
                        coorYinitiale = coorY;
                    }
                    if (humain.tabBatCoule[humain.tabBateau[coorX][coorY]]==9) // Si le bateau des coordonn�es en cours est coul�, on sort de la strat�gie
                    {
                        strategieEnCours = 0;
                        // Strat�gie, on regarde si il reste un T apr�s avoir coul� un bateau, si on on relance la strat�gie avec point de coordonn�e itinitiale le T
                        resteT = resteTinGrille(humain.tabGrille,&coorXinitiale,&coorYinitiale);

                        if(resteT == 1 )
                        {
                            strategieEnCours = 1;
                        }
                    }
                }
                REFRESH_CONSOLE();
            }
            if (humain.gagne == 1)
            {
                humain.nbrParties = humain.nbrParties + 1;
                humain.nbrPartiesGagnees = humain.nbrPartiesGagnees + 1;
                printf("\n\n BRAVO, VOUS AVEZ GAGNE  \n\n\n");
            }
            if (ordinateur.gagne == 1)
            {
                humain.nbrParties = humain.nbrParties + 1;
                printf("\n\n ORDINATEUR A GAGNE  \n\n\n");
            }
//ECRITURE DU PROFIL - NON FINALISE
            /*if(strcmp(humain.nom,"JOUEUR DEFAULT")==0) //le joueur vient d'une partie rapide
            {
                printf("\n\n Pour enregister le resultat, veuillez saisir votre nom (sinon saisir NON):  \n\n\n");
                scanf("%s",humain.nom);
                if (strcmp(humain.nom,"NON") == 0)
                {
                }
                else
                {
                    ecrireProfil(humain);
                }
            }
            else
            {
                ecrireProfil(humain);
            }
            REFRESH_CONSOLE();*/
            printf("\n\nRETOUR AU MENU\n\n\n");
            choixMenu = 0;
            break;
        case SELECTION_PROFIL :
            REFRESH_CONSOLE();
            chargerProfil(&humain);
            choixMenu = 1;
            break;
        case QUITTER :
            exit(0);
            break;
        default:
            REFRESH_CONSOLE();
            printf("\n\nERREUR de saisie, veuillez entrer le chiffre correspondant au menu.\n\n");
            menu(&choixMenu);
        }
    }
    return 0;
}
