#include "main.h"
/**************************************************************************************
Proc�dure   AFFICHERGRILLE
            Est appel�e � chaque tour, la fonction affiche/actualise les deux grilles.
            En entr�e, les structure permettent d'afficher le nom des joueurs ainsi que les
            statistiques des parties jou�es pr�cedemment.
            TEST d'une barre d'�volution des score.

i               int incr�mentation pour parcourir les lignes
j               int incr�mentation pour parcourir les colonnes
stat            int pourcentage statistique de parties gagn�es par la joueur
Joueur humain   structure qui va nous servir � afficher tabGrille, tabBateau, nbrPartieGagne, nbrPartie
Joueur ordinateur idem
tabLettre[11]   affichage des lettres de la 1ere colonne, appel� si i = 0;
compteurTir     affichage du nombre de tours

=> Entr�e       struct humain          Sortie =>   printf des grilles
                struct ordinateur
                tabLettre
                compteurTir
**************************************************************************************/
void afficherGrille(Joueur humain,Joueur ordinateur,char tabLettre[11],int compteurTir)
{
    int i = 0,j = 0;
    double stat = 0;

    printf("\n______________________________________________________________________________________________________________");
    printf("\n                                                    |                                                         ");
    printf("\n                 %s                                                     %s                       ",humain.nom,ordinateur.nom);
    printf("\n____________________________________________________|_________________________________________________________");
    printf("\n                                                    |                                                         \n");

    for(i=0; i<11; i++)
    {
        j=0;
        if (j==0) // Si j=0, alors c'est la premi�re colonne, on affiche alors une Lettre
        {
            printf(" %c|",tabLettre[i]);
        }
        for(j=1; j<11; j++)
        {
            if (i==0)
            {
                printf("%2d |",j);   // Si i=0, alors c'est la premi�re ligne, on affiche alors un chiffre
            }
            else
            {
                if (humain.tabGrille[i][j]=='T' || humain.tabGrille[i][j]=='.' || humain.tabGrille[i][j]=='C')
                {
                    printf(" %c |",humain.tabGrille[i][j]);
                }
                else
                {
                    if (humain.tabBateau[i][j]!=0)
                    {
                        printf(" %1d |",humain.tabBateau[i][j]);
                    }
                    else
                    {
                        printf("   |");
                    }
                }
            }
        }
// Ex�cution une seconde fois la boucle 'ligne', initialise j � 0, et on met un �cart entre les deux grilles.
        j=0;
        printf("         |      ");

        if (j==0)
        {
            printf(" %c|",tabLettre[i]);
        }
        for(j=1; j<11; j++)
        {
            if (i==0)
            {
                printf("%2d |",j);
            }
            else
            {
                if (ordinateur.tabGrille[i][j]=='T' || ordinateur.tabGrille[i][j]=='.' || ordinateur.tabGrille[i][j]=='C')
                {
                    printf(" %c |",ordinateur.tabGrille[i][j]);
                }
                else
                {
                    if (ordinateur.tabBateau[i][j]!=0)
                    {
                        printf(" %1d |",ordinateur.tabBateau[i][j]);
                    }
                    else
                    {
                        printf("   |");
                    }
                }
            }
        }
        printf("\n");
    }
    printf("\n                                           Nombre de tour(s):  %d                                             ",compteurTir);
    printf("\n\n______________________________________________________________________________________________________________");
    printf("\n                                              STATISTIQUES                                   ");
    printf("\n                                           Partie(s) jouee(s): %d                                                         ",ordinateur.nbrParties);

   //TEST FOR FUN AFFICHAGE BARRE SCORE
    stat = ((double)humain.nbrPartiesGagnees / (double)humain.nbrParties)*100;
    if(stat==0)
    {
        printf("                                         Partie(s) gagnee(s): %d| Loser                   |%d\n",humain.nbrPartiesGagnees,ordinateur.nbrPartiesGagnees);
    }
    if(stat<=20 && stat>0)
    {
        printf("                                         Partie(s) gagnee(s): %d| %c%c%c                  |%d\n",humain.nbrPartiesGagnees,254,254,254,ordinateur.nbrPartiesGagnees);
    }
    if(stat<=40 && stat>20)
    {
        printf("                                         Partie(s) gagnee(s): %d| %c%c%c%c%c              |%d\n",humain.nbrPartiesGagnees,254,254,254,254,254,ordinateur.nbrPartiesGagnees);
    }
    if(stat<=60 && stat>40)
    {
        printf("                                         Partie(s) gagnee(s): %d| %c%c%c%c%c%c%c          |%d\n",humain.nbrPartiesGagnees,254,254,254,254,254,254,254,ordinateur.nbrPartiesGagnees);
    }
    if(stat<=80 && stat>60)
    {
        printf("                                         Partie(s) gagnee(s): %d| %c%c%c%c%c%c%c%c%c%c    |%d\n",humain.nbrPartiesGagnees,254,254,254,254,254,254,254,254,254,254,ordinateur.nbrPartiesGagnees);
    }
    if(stat<100 && stat>80)
    {
        printf("                                         Partie(s) gagnee(s): %d| %c%c%c%c%c%c%c%c%c%c%c%c|%d\n ",humain.nbrPartiesGagnees,254,254,254,254,254,254,254,254,254,254,254,254,ordinateur.nbrPartiesGagnees);
    }
    if(stat==100)
    {
        printf("                                        Partie(s) gagnee(s): %d| %c%c%c%c%c%c%c%c%c%c%c%c|%d\n",humain.nbrPartiesGagnees,254,254,254,254,254,254,254,254,254,254,254,254,ordinateur.nbrPartiesGagnees);
    }
    printf("\n______________________________________________________________________________________________________________");
    printf("\n\n                                                                                     Pour quitter, saisir Q0");
}
/**************************************************************************************
Proc�dures  EFFETVERTBLANC et EFFETVIOLETBLANC
            Sont appel�es lorsque qu'un bateau est touch� (retour fonction tir = 1)
            Effet visuel suivant le joueur ou l'ordinateur

=> Entr�e       void          Sortie =>   void
**************************************************************************************/
void effetVertBlanc(void)
{
    COULEUR_CONSOLE_VERT_BLANC();
    Sleep(100);
    COULEUR_CONSOLE_BLANC_BLEU();
    Sleep(50);
    COULEUR_CONSOLE_VERT_BLANC();
    Sleep(100);
    COULEUR_CONSOLE_BLANC_BLEU();
}
void effetVioletBlanc(void)
{
    COULEUR_CONSOLE_VIOLET_BLANC();
    Sleep(100);
    COULEUR_CONSOLE_BLANC_BLEU();
    Sleep(50);
    COULEUR_CONSOLE_VIOLET_BLANC();
    Sleep(100);
    COULEUR_CONSOLE_BLANC_BLEU();
}
