#include "main.h"
/**************************************************************************************
Proc�dure   STRATEGIEORDINATEUR
            Est appel�e si strat�gie ordinateur est � 1 dans le main.c.
            C'est � dire si lorsqu'un tir est ok jusqu'� que le bateau soit coul�.
            OU si il reste un T dans la grille, on le d�tecte, on repars de ce "T" restant.

ptr_coorX           coordonn�es X
ptr_coorY           coordonn�es Y
ptr_coorXinitiale   coordonn�es X lors du premier tir, point de r�f�rence
ptr_coorYinitiale   coordonn�es Y lors du premier tir, point de r�f�rence
strategieEnCours    compteur de strat�gie en cours, on n'effectue pas les m�mes action si
                    1er tour ou non
toucheStrategie     retour de la fonction TIR, indique sur touche ou non en restant dans la
                    strat�gie
tabGrille           on utilise la grille pour anticiper des tir sur des T, C ou .
tabBateau           tabBateau
directionTirInitiale
directionTir
dirX
dirY
tabBatCoule
debordement
changementDir

=> Entr�e       ptr_coorX       Sortie =>   modification ptr_coorX
                ptr_coorY                   modification ptr_coorY
                ptr_coorXinitiale               + modification si besoin
                ptr_coorYinitiale               directionTir
                strategieEnCours                dirX
                toucheStrategie                 dirY
                tabGrille
                tabBateau
                directionTir
                dirX
                dirY
                tabBatCoules
**************************************************************************************/
void strategieOrdinateur(int *ptr_coorX,int *ptr_coorY,int *ptr_coorXinitiale,int *ptr_coorYinitiale,int strategieEnCours,int toucheStrategie,char tabGrille[11][11],int tabBateau[11][11],int *directionTir,int *dirX,int *dirY,int tabBatCoule[6])
{
    int debordement = 1;
    int changementDir = 0;

    if (tabBatCoule[tabBateau[*ptr_coorXinitiale][*ptr_coorYinitiale]] == 1) // Premier passage, tir random autour du coorXYintiale
    {
        while(debordement == 1)
        {
            *dirX = 0;
            *dirY = 0;
            *directionTir = rand()%4;
            direction_f2(&dirX,&dirY,&directionTir);
            *ptr_coorX = *ptr_coorXinitiale+*dirX;
            *ptr_coorY = *ptr_coorYinitiale+*dirY;
            debordement = testDebordement(*ptr_coorX,*ptr_coorY,tabGrille);
        }
    }
    else // Nous sommes alors dans le deuxi�me passage ou plus de la strat�gie
    {
        if (toucheStrategie == 1) //V�rifie que tir=OK en strat�gie, sinon, il faudra changer de direction
        {
            direction_f2(&dirX,&dirY,&directionTir);
            if (changementDir==0)
            {
                *ptr_coorX = *ptr_coorXinitiale+*dirX;
                *ptr_coorY = *ptr_coorYinitiale+*dirY;
            }
            else
            {
                *ptr_coorX = *ptr_coorXinitiale-*dirX;
                *ptr_coorY = *ptr_coorYinitiale-*dirY;
            }
            debordement = testDebordement(*ptr_coorX,*ptr_coorY,tabGrille);
            if(debordement == 1)
            {
                if (changementDir==0)
                {
                    *dirX = 0;//EMPECHE D INCREMENTER LE CHANGEMENT DE DIRECTION
                    *dirY = 0;
                    direction_f2(&dirX,&dirY,&directionTir);
                    *ptr_coorX = *ptr_coorXinitiale-*dirX;
                    *ptr_coorY = *ptr_coorYinitiale-*dirY;
                    changementDir++;
                }
                else
                {
                    direction_f2(&dirX,&dirY,&directionTir);
                    *ptr_coorX = *ptr_coorXinitiale-*dirX;
                    *ptr_coorY = *ptr_coorYinitiale-*dirY;
                }
            }
        }
        else
        {
            if(changementDir==0)
            {
                *dirX = 0;
                *dirY = 0;
                changementDir++;
            }
            direction_f2(&dirX,&dirY,&directionTir);
            *ptr_coorX = *ptr_coorXinitiale-*dirX;
            *ptr_coorY = *ptr_coorYinitiale-*dirY;
            debordement = testDebordement(*ptr_coorX,*ptr_coorY,tabGrille);
            while(debordement == 1)
            {
                *dirX = 0;
                *dirY = 0;
                *directionTir = rand()%4;
                direction_f2(&dirX,&dirY,&directionTir);
                *ptr_coorX = *ptr_coorXinitiale-*dirX;
                *ptr_coorY = *ptr_coorYinitiale-*dirY;
            }
        }
    }
}

/**************************************************************************************
Fonction    DEBORDEMENT
            Test si pour deux coordonn�es entr�es, celles-ci d�passent de la grille
            ou tombe sur T ou C ou . Renvoi alors 1 sur les coordonn�es ne correspondent
            pas.

Variables   coorX           coordonn�e X (saisie par l'utilisateur ou d�finie par l'ordi.)
            coorY           coordonn�e Y (saisie par l'utilisateur ou d�finie par l'ordi.)
            tabGrille       Grille

=> Entr�e       coorX               Sortie =>   Retourne 1 si deborde
                coorY                           Sinon 0
                tabGrille
**************************************************************************************/
int testDebordement(int coorX,int coorY,char tabGrille[11][11])
{
    if(tabGrille[coorX][coorY] == '.' || tabGrille[coorX][coorY] == 'T' || tabGrille[coorX][coorY] == 'C' || coorX < 1 || coorY < 1 || coorX > 10 || coorY > 10 )
    {
        return 1;
    }
    return 0;
}
/**************************************************************************************
Fonction    RESTETINGRILLE
            Est appel�e � chaque fois qu'un bateau est coul�. On contr�le si il ne reste
            pas de T. Si ou, c'est qu'il y avait 2 bateaux coll�s. On repart alors
            sur une strat�gie.

Variables   tabGrille           Grille � parcourir
            ptr_coorXinitiale   coordonn�e X
            ptr_coorYinitiale   coordonn�e Y
            i,j                 incr�ment� pour boucles for
            resteT              permet de mettre condition if pour le return

=> Entr�e       tabGrille               Sortie =>   Retourne 1 si il reste T
                ptr_coorXinitiale                   Sinon 0
                ptr_coorYinitiale
**************************************************************************************/
int resteTinGrille(char tabgrille[11][11],int *ptr_coorXinitiale,int *ptr_coorYinitiale)
{
    int i = 0, j = 0;
    int resteT = 0;
    for(i=0; i<11; i++)
    {
        for(j=1; j<11; j++)
        {
            if(tabgrille[i][j] == 'T')
            {
                *ptr_coorXinitiale=i;
                *ptr_coorYinitiale=j;
                resteT = 1;
                break;
            }
        }
    }
    if(resteT == 1) //On met le return � ce moment pour ne pas �crire � chaque fois que l'on parcours la boucle
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
/**************************************************************************************
Fonction    DIRECTION(2)
            idem que l'autre direction sauf que l'on passe des pointeurs en argument.
**************************************************************************************/
void direction_f2(int **p_i_vertical,int **p_i_horizontal,int **i_direction)
{
    switch (**i_direction)
    {
    case 0 :
        **p_i_vertical = **p_i_vertical + 1;
        break;
    case 1 :
        **p_i_vertical = **p_i_vertical - 1;
        break;
    case 2 :
        **p_i_horizontal = **p_i_horizontal + 1;
        break;
    case 3 :
        **p_i_horizontal = **p_i_horizontal - 1;
        break;
    default:
        printf("Erreur dans switch direction");
    }
}
