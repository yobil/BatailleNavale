/* -------------------------------------------------------------------------------------------------------
BATAILLE NAVALE
Version 1.0
Yoann BILIATO
2015/2016 Cycle ELAN Grenoble INP
 ---------------------------------------------------------------------------------------------------------*/
#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>

// global
#define REFRESH_CONSOLE(); system("cls");
#define CLEAN_BUFFER(); clean();
#define COULEUR_CONSOLE_BLANC_BLEU(); system("COLOR F3");
#define COULEUR_CONSOLE_VIOLET_BLANC(); system("COLOR 5F");
#define COULEUR_CONSOLE_VERT_BLANC(); system("COLOR 2F");

// menu
#define PARTIE_RAPIDE 1
#define SELECTION_PROFIL 2
#define QUITTER 5

// tir
#define TOUCHE_OK 1
#define RATE 0

// main
#define EXECUTE_WHILE 1
#define QUITTER_WHILE 0

//Structure du joueur ou de l'ordinateur
typedef struct Joueur Joueur;
struct Joueur
{
    char nom[20];
    int tabBateau[11][11];
    char tabGrille[11][11];
    int tabBatCoule[6];
    int gagne;
    int nbrParties;
    int nbrPartiesGagnees;
};

//Saisies.c
void menu(int *ptr_choixMenu);
void saisieDesCoordonnees(char *ptr_coorC, int *ptr_coorY, char tabLettre[11]);
int transformCoorCharToInt(char tabLettre[11],char coorC);
void clean(void); //Proc�dure pour clean le buffer suite � la saisie scanf, sinon boucle while s'�xecute deux fois (prise sur openclasseroom)
//affichage.c
void afficherGrille(Joueur humain,Joueur ordinateur,char p_a_lettre[11], int compteurTir);
void effetVertBlanc(void);
void effetVioletBlanc(void);
//strategie.c
void strategieOrdinateur(int *ptr_coorX,int *ptr_coorY,int *ptr_coorXinitiale,int *ptr_coorYinitiale,int strategieEnCours,int toucheStrategie,char tabGrille[11][11],int tabBateau[11][11],int *directionTir,int *dirX,int *dirY,int tabBatCoule[6]);
int testDebordement(int coorX,int coorY,char tabGrille[11][11]);
int resteTinGrille(char tabgrille[11][11],int *coorXinitiale,int *coorYinitiale);
void direction_f2(int **p_i_vertical,int **p_i_horizontal,int **i_direction);
//fonctions.c
void creationDesBateaux(int (*ptr_tabBateau)[11]);
int tir(int coorX,int coorY,int tabBateau[11][11],char (*ptr_tabGrille)[11],int *ptr_tabBatCoule);
void writeC(char (*ptr_tabGrille)[11],int tabBateau[11][11], int numBateau);
void direction_f(int *p_i_vertical,int *p_i_horizontal,int i_direction); //Fonction � revoir
void coordonneesAleatoire(int *ptr_coorX, int *ptr_coorY,char tabGrille[11][11]);
int verifGagne(int tabBatCoule[6]);
int selectionDifficulte(void);
//Initialisation pour partie � la suite
void initGrille(char (*ptr_tabGrille)[11]);
void initBateau(int (*ptr_tabBateau)[11]);
void initTabCoule(int *ptr_tabBatCoule);
void initTabStockCoups(int *ptr_tabStockCoups);
//profile.c EN CONSTRUCTION = Charge OK, Ecriture NOK
void chargerProfil(struct Joueur* humain);
void ecrireProfil(Joueur humain);
#endif // MAIN_H_INCLUDED
