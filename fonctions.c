#include "main.h"
/**************************************************************************************
Proc�dure   CREATIONDESBATEAUX (source: me)
            Est appel� 2 fois au d�but de la partie pour cr�er les bat. joueur et ordi.
            Cr�er les 5 bateaux du plus grand au plus petit avec contr�le du
            placement pour le d�bordement du tableau ou sur un autre bateau.

            Possibilit� d'am�lioration: utilisation d'un malloc pour stocker les coordonn�es
            de bon placement lorsque l'on fait notre test. Ensuite, on a juste � �crire ces
            coordonn�es au lieu de refaire un boucle?

ptr_tabBateau   tableau int des bateaux (� l'entr�e {0}, � la sortie bateau �crit)
i               int incr�mentation
bateau          int du bateau � placer (1 � 5)
longueurBateau  n�cessaire car la longueur du bateau n'est pas forcement son num�ro
ecrireBateau    int indicateur passe 1 lorsque l'on peut �crire un bateau en entier
CompteBonnesCases compteur de bonnes cases lors du placement, si = longueurBat on �crit
direction       indicateur de la direction � prendre
dirX            int qui s'incr�mente suivant la direction
dirY            int qui s'incr�mente suivant la direction
coorX           coordon�e X du premier placement, g�n�r�e par rand
coorY           coordon�e Y du premier placement, g�n�r�e par rand

=> Entr�e       tabBateau          Sortie =>   modification de ptr_tabBateau
**************************************************************************************/
void creationDesBateaux(int (*ptr_tabBateau)[11])
{
    //srand(time(NULL));  ERREUR C'est cette ligne qui m'a cr��e deux fois le m�me tableau, mais pas en mode debug..
    int i = 0;
    int bateau = 0;
    int longueurBateau = 0;
    int ecrireBateau = 0;
    int CompteBonnesCases = 0;
    int direction = 0;
    int dirX = 0,dirY = 0;
    int coorX = 0, coorY = 0;

    for(bateau=5; bateau>0; bateau--) // Execute 5 fois pour placer les 5 bateaux, On commence par le bateau le plus grand pour optimiser le temps de placement
    {
        ecrireBateau = 0;
        switch (bateau) // On change de bateau � chaque passe, on commence par la bateau le plus grand
        {
        case 1 :
            longueurBateau = 2;
            break;
        case 2 :
            longueurBateau = 3;
            break;
        case 3 :
            longueurBateau = 3;
            break;
        case 4 :
            longueurBateau = 4;
            break;
        case 5 :
            longueurBateau = 5;
            break;
        default:
            printf("ERREUR Switch  bateau");
        }
        while (ecrireBateau == 0) //On ex�cute la boucle tant qu'on n'a pas trouv� un place, et donc �crit un bateau
        {
            direction = rand()%4;
            coorX = 1 + rand()%10;
            coorY = 1 + rand()%10;
            dirX = 0;
            dirY = 0;
            CompteBonnesCases = 0;

            for(i=0; i<longueurBateau; i++)
            {
                //On test si la coordonn�e dans tableau bateau = 0, et si on ne d�borde pas du tableau. SI ces cas sont respect�s, on incr�mente notre compte de bonnes cases
                if (ptr_tabBateau[coorX+dirX][coorY+dirY] == 0 && coorY+dirY > 0 && coorX+dirX > 0  && coorY+dirY < 11  && coorX+dirX < 11 )
                {
                    CompteBonnesCases++;
                    direction_f(&dirX,&dirY,direction); //On appel direction qui incr�mente la direction choisie.
                }
                else // On sort de la boucle tout de suite si la case n'est pas bonne
                {
                    break;
                }
            }
            if (CompteBonnesCases == longueurBateau) //Si le nombre de bonnes cases correspondent � la longueur du bateau, on peut �crire le bateau
            {
                dirX = 0; //Re-initialise vertical et horizontal pour re-oartir du point initial
                dirY = 0;
                for(i=0; i<longueurBateau; i++)
                {
                    ptr_tabBateau[coorX+dirX][coorY+dirY] = bateau;
                    direction_f(&dirX,&dirY,direction);
                }
                ecrireBateau = 1; // On sort de la boucle while
            }
        }
    }
}
/**************************************************************************************
Fonction    TIR (source: me)
            Si dans le tableau Bateau c'est une valeur diff�rent de 0, alors la fonction
            retourne 1, on �crit un T dans le tableau Grille et on incr�mente le tabBatCoule
            du bateau correspondant.
            Ensuite on v�rifie si celui-ci est coul�. On compare alors le compteur du
            tabBatCoule � la longueur du bateau. Si les valeurs sont �gales, on �crit C, et on
            �crit la valeur du tabBatCoule � 9 pour reconnaitre que la bateau est coul�.
            Sinon, la fonction retourne 0 et �crit un . dans le tableau Grille

Variables   coorX           coordonn�e X (saisie par l'utilisateur ou d�finie par l'ordi.)
            coorY           coordonn�e Y (saisie par l'utilisateur ou d�finie par l'ordi.)
            tabBateau       Tableau des bateaux
            ptr_tabGrille   Tableau des grilles, sera modifier par 'T', '.' ou 'C'
            prt_tabBatCoule Tableau des bat. coul�s, stock "l'�tat de bateaux"
            longueurBateau  Longueur des bateaux

=> Entr�e       coorX               Sortie =>   modification ptr_tabGrille
                coorY                           modification ptr_tabBatCoule
                tabBateau                       Retourne 1 ou 0
                ptr_tabGrille
                ptr_tabBatCoule
**************************************************************************************/
int tir(int coorX,int coorY,int tabBateau[11][11],char (*ptr_tabGrille)[11],int *ptr_tabBatCoule)
{
    int longueurBateau;
    if(tabBateau[coorX][coorY]!=0)
    {
        ptr_tabGrille[coorX][coorY] = 'T';
        ptr_tabBatCoule[tabBateau[coorX][coorY]]++;

        switch (tabBateau[coorX][coorY]) //Pour �criture des C si bateau est coul�
        {
        case 1 :
            longueurBateau = 2;
            break;
        case 2 :
            longueurBateau = 3;
            break;
        case 3 :
            longueurBateau = 3;
            break;
        case 4 :
            longueurBateau = 4;
            break;
        case 5 :
            longueurBateau = 5;
            break;
        default:
            printf("ERREUR Switch  bateau");
        }
        if (ptr_tabBatCoule[tabBateau[coorX][coorY]] == longueurBateau) // Bateau 1 est coul� si touch� 2 fois
        {
            writeC(ptr_tabGrille,tabBateau,tabBateau[coorX][coorY]);
            ptr_tabBatCoule[tabBateau[coorX][coorY]] = 9; // Valeur � 9, le bateau est coul�, et on ne repassera plus par ce if
        }
        return TOUCHE_OK;
    }
    else
    {
        ptr_tabGrille[coorX][coorY] = '.';
        return RATE;
    }
}
/**************************************************************************************
Proc�dure   WRITEC (source: me)
            Est appel� si un bateau est coul�. Parcours le tabBateau pour �crit 'C' la ou
            le num�ro correspond au bateau  coul�.

Variables   numBateau       Num�ro du bateau coul�
            tabBateau       Tableau des bateaux
            ptr_tabGrille   Tableau des grilles, sera modifier par 'C'

=> Entr�e   ptr_tabGrille             Sortie =>   modification ptr_tabGrille
            tabBateau
            numBateau
**************************************************************************************/
void writeC(char (*ptr_tabGrille)[11],int tabBateau[11][11], int numBateau)
{
    int i = 0, j = 0;
    for(i=0; i<11; i++)
    {
        for(j=1; j<11; j++)
        {
            if(tabBateau[i][j] == numBateau)
            {
                ptr_tabGrille[i][j]='C';
            }
        }
    }
}
/**************************************************************************************
Proc�dure   DIRECTION (source: me)
            A une direction correspond une addition ou une soustraction horizontale ou verticale

Variables   numBateau       Num�ro du bateau coul�
            tabBateau       Tableau des bateaux
            ptr_tabGrille   Tableau des grilles, sera modifier par 'C'

=> Entr�e   ptr_tabGrille             Sortie =>   modification ptr_tabGrille
            tabBateau
            numBateau
**************************************************************************************/
void direction_f(int *p_i_vertical,int *p_i_horizontal,int i_direction)
{
    switch (i_direction)
    {
    case 0 :
        *p_i_vertical = *p_i_vertical + 1;
        break;
    case 1 :
        *p_i_vertical = *p_i_vertical - 1;
        break;
    case 2 :
        *p_i_horizontal = *p_i_horizontal + 1;
        break;
    case 3 :
        *p_i_horizontal = *p_i_horizontal - 1;
        break;
    default:
        printf("Erreur dans switch direction");
    }
}
/**************************************************************************************
Proc�dure   COORDONNEESALEATOIRE (source: me)
            Les coordonn�es sont re-g�n�r�es tant que les coordonn�es g�n�r�r�es
            al�atoirements correspondent � un tir r�alis� dans la grille (T ou C ou .)

Variables   ptr_coorX       Coordonn�es X
            ptr_coorY       Coordonn�es Y
            tabGrille   Tableau des grilles

=> Entr�e   ptr_coorX             Sortie =>     modification ptr_coorX
            ptr_coorY                           modification ptr_coorY
            tabGrille
**************************************************************************************/
void coordonneesAleatoire(int *ptr_coorX, int *ptr_coorY,char tabGrille[11][11])
{
    *ptr_coorX = 1 + rand()%10;
    *ptr_coorY = 1 + rand()%10;

    while ((tabGrille)[*ptr_coorX][*ptr_coorY] == 'C' || (tabGrille)[*ptr_coorX][*ptr_coorY] == 'T' || (tabGrille)[*ptr_coorX][*ptr_coorY] == '.')
    {
        *ptr_coorX = 1 + rand()%10;
        *ptr_coorY = 1 + rand()%10;
    }
}
/**************************************************************************************
Fonction    verifGagne (source: me)
            V�rification si joueur ou ordinateur � gagn�. Est appel� � chaque fois qu'un
            retour de TIR est positif

Variables   tabBatCoule Tableau des bateaux coul�s

=> Entr�e   tabBatCoule             Sortie =>     Retourne 1 ou 0
**************************************************************************************/
int verifGagne(int tabBatCoule[6])
{
    if (tabBatCoule[1] == 9)
    {
        return 1;
    }
    return 0;
}
/**************************************************************************************
Fonction    SELECTIONDIFFICULTE (source: me)
            Affichage et selection de la difficulte par l'utilisateur.
            Suivant le niveau s�lectionn�, l'ordinateur connait une coordon�e
            de 1 ou plusieurs bateaux.

coorC           char saisie auparavant par l'utilisateur
tabLettre[11]   tableau comprenant les lettres des colonnes

=> Entr�e       coorC           Sortie =>   int (position coorC dans tabLettre)
                tabLettre[11]
**************************************************************************************/
int selectionDifficulte(void)
{
    int selection = 0,difficulte = 1;
    while(difficulte == 1)
    {
        printf("\n SELECTION DIFFICULTE");
        printf("\n 0 - FACILE           Le radar de l'ordinateur a detecte 0 de  vos bateaux.");
        printf("\n 1 - MOYEN -          Le radar de l'ordinateur a detecte 1 de  vos bateaux.");
        printf("\n 2 - MOYEN +          Le radar de l'ordinateur a detecte 2 de  vos bateaux.");
        printf("\n 3 - DIFFICILE        Le radar de l'ordinateur a detecte 3 de  vos bateaux.");
        printf("\n 4 - TRES DIFFICILE   Le radar de l'ordinateur a detecte 4 de  vos bateaux.");
        printf("\n 5 - EXTREME          Le radar de l'ordinateur a detecte 5 de  vos bateaux.");
        printf("\n\n Votre choix:");
        scanf("%1i",&selection);
        CLEAN_BUFFER();
        switch (selection)
        {
        case 0 :
            difficulte= 0;
            break;
        case 1 :
            difficulte= 2;
            break;
        case 2 :
            difficulte= 4;
            break;
        case 3 :
            difficulte= 6;
            break;
        case 4 :
            difficulte= 8;
            break;
        case 5 :
            difficulte= 10;
            break;
        default:
            printf("\nErreur dans switch selection difficulte\n");
        }
    }
    return difficulte;
}
/**************************************************************************************
Proc�dure   INITIALISERGRILLE (source: me)
            Initialisation des valeurs de la grille � ''
Variables
=> Entr�e   ptr_tabGrille             Sortie =>   modification ptr_tabGrille
            tabBateau
            numBateau
**************************************************************************************/
void initGrille(char (*ptr_tabGrille)[11])
{
    int i = 0, j = 0;
    for(i=0; i<11; i++)
    {
        for(j=1; j<11; j++)
        {
            ptr_tabGrille[i][j]=' ';
        }
    }
}
/**************************************************************************************
Proc�dure   INITIALISERBATEAU (source: me)
            Initialisation des valeurs de la matrice bateau � 0
Variables
=> Entr�e   ptr_tabGrille             Sortie =>   modification ptr_tabGrille
            tabBateau
            numBateau
**************************************************************************************/
void initBateau(int (*ptr_tabBateau)[11])
{
    int i = 0, j = 0;
    for(i=0; i<11; i++)
    {
        for(j=1; j<11; j++)
        {
            ptr_tabBateau[i][j]=0;
        }
    }
}
void initTabCoule(int *ptr_tabBatCoule)
{
    int i = 0;
    for(i=0; i<6; i++)
    {
        ptr_tabBatCoule[i]=0;
    }
}
void initTabStockCoups(int *ptr_tabStockCoups)
{
    int i = 0;
    for(i=0; i<11; i++)
    {
        ptr_tabStockCoups[i]=0;
    }
}

