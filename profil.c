#include "main.h"
/**************************************************************************************
Proc�dure   CHARGERPROFIL (source: me)
            Lit le fichier stockProfile.txt et affiche les profils enregistr�s. Le joueur
            choisit sont profil, on initialise 3 valeurs pour le d�but du programme.

Joueur* humain  pointeur sur structure pour modifier, nom, nbrPartieGagne, nbrPartie
choixProfil     int du choix du profil fait
nom[20] = ""    char provisoire qui va �tre ensuite �crit dans la structure
joue            int provisoire qui va �tre ensuite �crit dans la structure
gagne           int provisoire qui va �tre ensuite �crit dans la structure
i               int pour boucles de parcours des lignes du fichier

=> Entr�e       Joueur* humain          Sortie =>   modification de
                                                    humain.nom
                                                    humain.nbrParties
                                                    humain.nbrPartiesGagne
**************************************************************************************/
void chargerProfil(struct Joueur* humain)
{
    int  choixProfil = 0;
    char nom[20] = "";
    int joue = 0,  gagne = 0;
    int i = 1;
    FILE* fichier = fopen("stockProfil.txt","r+");
    if (fichier != NULL)
    {
        printf("\n\nSelectionner votre profil:\n");
        while(!feof(fichier))
        {
            if (fscanf(fichier, "%s%d%d", nom, &joue,&gagne) == 3) //�vite l'affichage en double de la derni�re ligne ou de ligne erron� en testant si retour fscanf = 3
            {
                printf("\n%d-%s", i,nom);
                i++;
            }
        }
    }
    else
    {
        printf("impossible d'ouvrir le fichier");
    }
    printf("\n\nVotre choix:");
    scanf("%1d",&choixProfil);
    i=1;
    rewind(fichier);
    while(!feof(fichier))
    {
        if (fscanf(fichier, "%s%d%d", nom, &joue,&gagne) == 3)
        {
            if (i == choixProfil)
            {
                strcpy(humain->nom,nom);
                humain->nbrParties = joue;
                humain->nbrPartiesGagnees = gagne;
            }
            i++;
        }
    }
    fclose(fichier);
}
/**************************************************************************************
Proc�dure   ECRITUREDUPROFIL (source: me)
            Est appel� � la fin d'une partie. Lit le nom saisie par l'utilisateur, si celui
            ci existe d�j�, il demande la saisie d'un nouveau nom
            Si le nom n'existe pas, on le rajoute � la fin
            Si il existe on peut �craser la ligne
            Si le joueur est d�j� charg�, transparant,on �crase automatiquement la ligne

Joueur humain   structure avec infos � enregistrer, nom, nbrPartieGagne, nbrPartie
nom[20] = ""    char provisoire pour comparer les noms du fichier au nom du joueur
ecraseOuNouveau int ecrase ou non la ligne actuelle

=> Entr�e       Joueur humain          Sortie =>    �criture du fichier avec
                                                    humain.nom
                                                    humain.nbrParties
                                                    humain.nbrPartiesGagne
**************************************************************************************/
void ecrireProfil(Joueur humain)
{
    char **tabNomProfil = NULL; //** ?
    int *tabNbrPartiesProfil = 0;
    int *tabNbrPartiesGagneesProfil = 0;
   /* char *nom = NULL;
    nom = (char *)malloc(sizeof(char)); //Permet d'avoir un nom de la longueur de la chaine sans les 00000*/
    char nom[20] = " ";
    int part1 = 0, part2 = 0;
    int i;
    int ecraseOuNouveau = 0;
    char c = NULL;
    int nbrLignes = 0;
// VERIFIER QUE LE PROFIL EXISTE DEJA OU NON
    FILE* fichier = fopen("stockProfil.txt","r+");
    if (fichier != NULL)
    {
        while((c=fgetc(fichier)!= EOF))
        {
            if (c == '\n' )
                nbrLignes++;
        }
        tabNomProfil = (char **) malloc(nbrLignes *sizeof(char*)); //** ??
        tabNbrPartiesProfil = (int *) malloc(nbrLignes *sizeof(int));
        tabNbrPartiesGagneesProfil = (int *) malloc(nbrLignes *sizeof(int));
        if(tabNomProfil == NULL || tabNbrPartiesProfil == NULL || tabNbrPartiesGagneesProfil == NULL)
        {
            printf("Erreur Allocation dynamique");
            exit(0);
        }
        rewind(fichier);
        while(fscanf(fichier, "%s%d%d", nom, &tabNbrPartiesProfil[i],&tabNbrPartiesGagneesProfil[i]) == 3)
        {
            tabNomProfil[i] = (char*) malloc(strlen(nom));
            strcpy( tabNomProfil[i],nom);
           // printf("\n %s",tabNomProfil[i]);
           // printf("\n %s",humain.nom);
            if(strchr(nom,*humain.nom))
                 //if(strchr(tabNomProfil[i],*humain.nom))
            {
                tabNbrPartiesProfil[i] = humain.nbrParties;
                tabNbrPartiesGagneesProfil[i] = humain.nbrPartiesGagnees;
                part1 = tabNbrPartiesProfil[i];
            }
            i++;
        }
        rewind(fichier);
        i = 3;
        //Ecrire dans le fichier
        for(i=2;i<=5;i++)
            {
                //nom = tabNomProfil[i];
                part1 = tabNbrPartiesProfil[i];
                part2 = tabNbrPartiesGagneesProfil[i];

               fprintf(fichier, "%s %d %d\n", nom, part1,part2);
            }
        if(ecraseOuNouveau != 1)
        {
            fprintf(fichier, "%s %d %d\n", humain.nom, humain.nbrParties,humain.nbrPartiesGagnees);
        }
    }
    else
    {
        printf("impossible d'ouvrir le fichier");
    }
    fclose(fichier);
         free(tabNomProfil);
         free(tabNbrPartiesGagneesProfil);
         free(tabNbrPartiesGagneesProfil);
}
