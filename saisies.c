#include "main.h"
/**************************************************************************************
Proc�dure   MENU
            Est compris dans une boucle while, s'affichera tant que l'utilisateur n'a pas
            saisie 5 "Quitter".

ptr_choixMenu   int saisie par l'utilisateur

=> Entr�e       ptr_choixMenu       Sortie =>   modification ptr_choixMenu
**************************************************************************************/
void menu(int *ptr_choixMenu)
{
    printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c ",201,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,187);
    printf("\n%c         BATAILLE NAVALE        %c\n",186,186);
    printf("%c      1   Partie rapide         %c\n",186,186);
    printf("%c      2   Partie avec Profil    %c\n",186,186);
    printf("%c      5   Quitter               %c\n",186,186);
    printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c ",200,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,205,188);
    //printf("\n\n                                        La creation d'un nouveau profil se realise a la fin d'une partie. ");
    printf("\n\n    Veuillez entrer votre choix:  ");
    scanf("%i", ptr_choixMenu);

    CLEAN_BUFFER(); //Clean apr�s un scanf
}
/**************************************************************************************
Proc�dure   SAISIEDESCOORDONNEES
            Le joueur saisie les 2 coordonn�es pour effectuer un tir. La fonction v�rifie
            que la lettre correspond � une ligne et le chiffre � une colonne.

ptr_coorC       char saisie par l'utilisateur
ptr_coorY       int saisie par l'utilisateur
tabLettre[11]   tableau comprenant les lettres des colonnes

 => Entr�e      ptr_coorC       Sortie =>   modification ptr_coorC
                ptr_coorY                   modification ptr_coorY
                tabLettre[11]
**************************************************************************************/
void saisieDesCoordonnees(char *ptr_coorC, int *ptr_coorY, char tabLettre[11])
{
    int saisieOk = 0;
    while (saisieOk == 0)
    {
        printf("\n Veuillez saisir les coordonnees:  ");
        scanf("%1c %2i", ptr_coorC, ptr_coorY);
        CLEAN_BUFFER();
        if (strchr(tabLettre,*ptr_coorC) && *ptr_coorY < 11 && *ptr_coorY > 0)
        {
            saisieOk = 1;
        }
        else if (*ptr_coorC == 'Q')
        {
            printf("\n    Quitter la partie en cours  \n");
            saisieOk = 1;
        }
        else
        {
            printf("\n    MAUVAISE SAISIE  \n");
            saisieOk = 0;
        }
    }
}
/**************************************************************************************
Fonction    TRANSFORMCOORCHARTOINT
            Compare la lettre saisie par le joueur dans le tableau lettre et renvoi la
            valeur correspondante (avec num�ro ref. pris sur l'incr�mentation de la
            boucle)

coorC           char saisie auparavant par l'utilisateur
tabLettre[11]   tableau comprenant les lettres des colonnes

=> Entr�e       coorC           Sortie =>   int (position coorC dans tabLettre)
                tabLettre[11]
**************************************************************************************/
int transformCoorCharToInt(char tabLettre[11],char coorC)
{
    int i;
    for(i=0; i<11; i++)
    {
        if (coorC == tabLettre[i])
        {
            return i;
        }
    }
    return 0;
}
/**************************************************************************************
Fonction    CLEAN (source externe: openClassRoom)
            efface le buffer
**************************************************************************************/
void clean(void)
{
    int c=0;
    while ((c=getchar()) != '\n' && c != EOF);
}
